#!/usr/bin/env bash

# produce a file with theory sections included
lualatex main.tex
cp main.pdf with-theory.pdf

# produce a file with theory sections excluded
lualatex "\def\skipTheory{1} \input{main.tex}"
cp main.pdf without-theory.pdf
