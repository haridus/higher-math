# A short summery of the higher mathematics course

This LaTeX project provides two versions of the conspectus: the first one  includes theory, the other one only formulas.

## Building

We assume you are using a UNIX style operating system. 

First install a LaTeX distribution and make sure that `lualatex` is in your `PATH`.

Then make the `build.sh` script executable (`chmod +x build.sh`) and run it. It produces two PDF files: `with-theory.pdf` and `without-theory.pdf`.

Feel free to open an issue or merge request for feedback and suggestions :)
